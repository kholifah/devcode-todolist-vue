# Devcode To-do List App

A to-do list web app that created when i joined [Devcode Frontend Challenge](https://devcode.gethired.id/challenge/vuejs-todolist) by [Gethired.id](https://gethired.id/). 

## Fetures
- Creating, updating, and removing Activity lists.
- Creating, updating, and removing Todos lists.
- Sorting Todos list. 

## Technologies

- [Vue.js](https://v2.vuejs.org/)
- [Vuex](https://vuex.vuejs.org/)
- [Vue-router](https://router.vuejs.org/introduction.html)
- [Vuetify](https://vuetifyjs.com/en/getting-started/installation/)
- [TailwindCSS](https://tailwindcss.com/)
- [Axios](https://axios-http.com/docs/intro)

## Project setup

- Clone this repository
- Run `yarn`
- Change userEmail value on index.js at store folder
- Run `yarn serve`
